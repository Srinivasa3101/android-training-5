package com.example.training5;

import com.google.gson.annotations.SerializedName;

public class GitHubRepo {
    @SerializedName("avatar_url")
    private String avatar;
    @SerializedName("login")
    private String name;

    public GitHubRepo(String avatar, String name) {
        this.avatar = avatar;
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }
}
