package com.example.training5;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImgViewHolder> {
    private ArrayList<GitHubRepo> imgItems;
    private Context context;

    public ImageAdapter(ArrayList<GitHubRepo> imgItems, Context context){
        this.imgItems = imgItems;
        this.context = context;
    }

    public static class ImgViewHolder extends  RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView textView;
        public  View view;
        private ShimmerFrameLayout shimmerFrameLayout;
        public LinearLayout shim;
        public  Context context;

        public ImgViewHolder(View itemView){
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            shimmerFrameLayout = itemView.findViewById(R.id.shimmer_view_container);
            shim = itemView.findViewById(R.id.shim);
            this.view = itemView;


        }
    }




    @NonNull
    @Override
    public ImgViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from((parent.getContext())).inflate(R.layout.imglist, parent, false);
        ImgViewHolder vh = new ImgViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ImgViewHolder holder, int position) {
        final GitHubRepo currentItem = imgItems.get(position);
        holder.textView.setText(currentItem.getName());
//        Picasso.with(context)
//        .load(currentItem.getAvatar())
//        .into(holder.imageView);

        Glide.with(context).load(currentItem.getAvatar()).placeholder(R.drawable.ic_launcher_foreground)
                .diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.imageView);

        holder.shimmerFrameLayout.stopShimmer();
        holder.shim.setVisibility(View.GONE);


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.github.com/" + currentItem.getName())));

            }
        });

    }

    @Override
    public int getItemCount() {
        return imgItems.size();
    }

}


