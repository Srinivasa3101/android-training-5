package com.example.training5;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubClient {

    @GET("/users")
    Call<ArrayList<GitHubRepo>> repoForUser();
    @GET("/users")
    Call<String> stringCall();
}
