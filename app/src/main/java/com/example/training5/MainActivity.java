package com.example.training5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ArrayList<GitHubRepo> imgItems = new ArrayList<>();
    private RecyclerView recyclerView;
    private ImageAdapter imgadapter;
    private RecyclerView.LayoutManager layoutManager;
    private ShimmerFrameLayout shimmerFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        imgadapter = new ImageAdapter(imgItems, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(imgadapter);

        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);

        //imgItems.add(new imgItem("https://avatars3.githubusercontent.com/u/5274298?v=4"));


        String API_BASE_URL = "https://api.github.com/";


        Retrofit.Builder builder = new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create()
                        );

        Retrofit retrofit = builder.build();

        GitHubClient client =  retrofit.create(GitHubClient.class);

        Call<ArrayList<GitHubRepo>> call = client.repoForUser();
//        Call<String> call = client.stringCall();
//        call.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                if (response.body() != null) {
//                    Log.e("RESOPNSE",response.body());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                if(t.getMessage() !=null)
//                Log.e("RESPONSE_ERR",t.getMessage());
//                t.printStackTrace();
//
//            }
//        });

        call.enqueue(new Callback<ArrayList<GitHubRepo>>() {
            @Override
            public void onResponse(Call<ArrayList<GitHubRepo>> call, Response<ArrayList<GitHubRepo>> response) {

                ArrayList<GitHubRepo> repos = response.body();

                if(repos !=null){
                    imgItems.addAll(repos);
                    imgadapter.notifyDataSetChanged();

                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                }
                else{
                    Log.e("RESPONSE",response.message());
                }


            }

            @Override
            public void onFailure(Call<ArrayList<GitHubRepo>> call, Throwable t) {
                Toast.makeText(getApplication(), "Error", Toast.LENGTH_SHORT).show();
            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        shimmerFrameLayout.stopShimmer();
        super.onPause();
    }


}