package com.example.training5;

public class imgItem {
    private String imageUrl;
    private String name;

    public imgItem(String imageUris, String names){
        this.imageUrl = imageUris;
        this.name = names;
    }

    public final String getImageUrl(){
        return imageUrl;
    }

    public final String getName(){
        return name;
    }
}
